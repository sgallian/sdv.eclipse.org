baseurl = "https://sdv.eclipse.org"
DefaultContentLanguage = "en"
title = "Software Defined Vehicle"
theme = "eclipsefdn-hugo-solstice-theme"
metaDataFormat = "yaml"
#googleAnalytics = "UA-910670-30"
disableKinds = []
themesDir = "node_modules/"
enableRobotsTXT = true
pluralizeListTitles = false

[Params]
  google_tag_manager = "GTM-5WLCZXC"
  description = "An open technology platform for the software defined vehicle of the future."
  seo_title_suffix = " | The Eclipse Foundation"
  keywords = ["Open source", "open technology platform", "automotive grade", "modular software components", "automation and virtualization", "open standards", "all vehicle domains", "safety and security standards", "time-to-market"]
  favicon = "/favicon.ico"
  logo = "images/logo/sdv-logo-color.svg"
  logo_width = "200"
  header_left_classes="col-sm-5 col-md-5"
  main_menu_wrapper_classes="col-sm-19 col-md-19 margin-top-40 margin-bottom-30"
  # share_img = ""
  styles = "public/css/styles.css"
  # gcse = "011805775785170369411:p3ec0igo0qq"
  js = "public/js/solstice.js"
  news_count = 3
  featured_content_publish_target = "sdv"
  container = "container-fluid"
  linkedin_url = "https://www.linkedin.com/showcase/software-defined-vehicle"
  twitter_url = "https://x.com/SDVeclipse"
  youtube_url = "https://www.youtube.com/@EclipseSDV"
  instagram_url = "https://www.instagram.com/eclipsesdv/"
  main_sidebar_class = "col-md-5 col-md-offset-1 padding-bottom-30 padding-right-0 hidden-sm hidden-xs"
  sidebar_layout = "sidebar_block"
  jumbotron_btn_class = "btn btn-neutral"
  layout_style = "quicksilver"

[taxonomies]
  category = "categories"
  tag = "tags"
  membership_level = "membership_levels"
  participation_level = "participation_levels"

[Author]
  name = "Eclipse Foundation"
  website = "https://www.eclipse.org"
  email = "webdev@eclipse-foundation.org"
  facebook = "eclipse.org"
  twitter = "sdveclipse"
  youtube = "EclipseFdn"
  googleplus = "+Eclipse"
  linkedin = "company/eclipse-foundation/"

[permalinks]
  news = "/:sections/:year/:month/:day/:slug/"

[blackfriday]
  plainIDAnchors = true
  hrefTargetBlank = true

[[menu.main]]
  name = "About"
  identifier = "about"
  weight = 1

[[menu.main]]
  name = "What is SDV"
  url = "/what-is-sdv"
  parent = "about"
  weight = 1

[[menu.main]]
  name = "About the Working Group"
  url = "/about-the-working-group"
  parent = "about"
  weight = 2

[[menu.main]]
  name = "Membership"
  identifier = "membership"
  weight = 2

[[menu.main]]
  name = "Our Members"
  url = "/members"
  parent = "membership"
  weight = 1

[[menu.main]]
  name = "Become a Member"
  url = "/become-a-member"
  parent = "membership"
  weight = 2

[[menu.main]]
  name = "Projects"
  url = "/projects"
  weight = 3

[[menu.main]]
  name = "SIGs"
  identifier = "sig"
  weight = 4

[[menu.main]]
  name = "What Are Special Interest Groups?"
  url = "/special-interest-groups/"
  parent = "sig"
  weight = 1

[[menu.main]]
  name = "Automotive Processes"
  url = "/special-interest-groups/automotive-processes/"
  parent = "sig"
  weight = 2

[[menu.main]]
  name = "Rust"
  url = "/special-interest-groups/rust/"
  parent = "sig"
  weight = 3

[[menu.main]]
  name = "ThreadX"
  url = "/special-interest-groups/threadx/"
  parent = "sig"
  weight = 4

[[menu.main]]
  name = "Community"
  identifier = "community"
  weight = 5

[[menu.main]]
  name = "Resources"
  url = "/resources/"
  parent = "community"
  weight = 1

[[menu.main]]
  name = "Get Engaged"
  url = "/get-engaged"
  weight = 5

[[menu.sidebar]]
  name = "Resources"
  identifier = "resources"
  url = "/resources/"
  weight = 5

[[menu.sidebar]]
  name = "News"
  identifier = "news"
  url = "/news/"
  weight = 1

[[menu.sidebar]]
  name = "Events"
  parent = "news"
  url = "/events/"
  pre = "<i data-feather=\"calendar\"></i>"
  weight = 1

[[menu.sidebar]]
  name = "Past Events"
  parent = "news"
  url = "/past-events/"
  pre = "<i data-feather=\"calendar\"></i>"
  weight = 1

[[menu.sidebar]]
  name = "Videos"
  parent = "resources"
  url = "/resources/videos"
  pre = "<i data-feather=\"video\"></i>"
  weight = 1

[[menu.sidebar]]
  name = "White Papers"
  parent = "resources"
  url = "/resources/white-papers"
  pre = "<i data-feather=\"file\"></i>"
  weight = 2

# Uncomment once survey reports get added at end of May 2024.
# https://gitlab.eclipse.org/eclipsefdn/it/websites/sdv.eclipse.org/-/issues/95#note_2134561
#
# [[menu.sidebar]]
#   name = "Survey Reports"
#   parent = "resources"
#   url = "/resources/survey-reports"
#   pre = "<i data-feather=\"pie-chart\"></i>"
#   weight = 3

[[menu.sidebar]]
  name = "Logos and Artwork"
  parent = "resources"
  url = "https://www.eclipse.org/org/artwork#sdv"
  pre = "<i data-feather=\"image\"></i>"
  weight = 6
