---
title: SDV Community Day at Mercedes-Benz Kundencenter Sindelfingen
date: 2024-01-30T15:29:46-04:00
summary: >-
    Meet and engage with the thriving SDV Community and work towards building
    an SDV Distribution entirely consisting of Open Source Projects.
description: >-
    Meet and engage with the thriving SDV Community and work towards building
    an SDV Distribution entirely consisting of Open Source Projects.
categories: []
keywords: []
headline: ''
custom_jumbotron: |
    <h1 class="featured-jumbotron-title text-center">SDV Community Day at Mercedes-Benz Kundencenter Sindelfingen</h1>
    <div class="more-detail jumbotron-register-container text-center">
        <div class="more-detail-text">
            <p>June 5-6, 2024</p>
        </div>
    </div>
hide_page_title: true
hide_headline: true
hide_sidebar: true
hide_breadcrumb: true
header_wrapper_class: header-community-day-2023
container: container-fluid 
page_css_file: /public/css/sdv-community-day-eclipsecon-2023.css
---

{{< grid/section-container class="featured-section-row text-center" isMarkdown="true" >}}

## More Information Coming Soon

We appreciate your interest and invite you to check back later for the latest
updates.

{{</ grid/section-container >}}
