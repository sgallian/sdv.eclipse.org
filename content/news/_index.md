---
title: 'News'
seo_title: 'News - Software Defined Vehicle'
description: ''
keywords: ['Software Defined Vehicle news']
date: 2022-03-14T10:00:00-04:00
container: 'container sdv-news'
---

{{< newsroom/news 
  id="news-list-container" 
  count="10"
  paginate="true"
  class="news-list margin-bottom-30" 
  publishTarget="sdv" 
  >}}
